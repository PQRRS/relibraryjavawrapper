package fr.PierreQR.RELibrary;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.PointerByReference;
import java.io.IOException;

public class Document {
    private static final Wrapper.NativeHelper reLib = Wrapper.NativeHelper.Instance;
    protected       Pointer                                 handle;
    protected       int                                     type                    = 0;

    public Document () throws Exception {
        handle = reLib.RECreateDocument();
        if (handle==null)
            throw new Exception("Failed to create a new Recognition Document.");
    }
    public void release () throws Exception {
        if (handle==null)
            return;
        reLib.REDeleteDocument(handle);
        handle = null;
    }
    @Override
    protected void finalize () throws Throwable {
        release();
        super.finalize();
    }

    public Pointer handle () {
        return handle;
    }

    public int type () {
        return type;
    }
    public void setType (int t) {
        type = t;
    }

    public String jsonResult (String catKey) {
        PointerByReference      bufferRef   = new PointerByReference();
        reLib.REGetDocumentJsonResultForKey (handle, catKey, bufferRef);
        String                  json        = bufferRef.getValue().getString(0);
        reLib.REReleaseString               (bufferRef.getValue());
        return json;
    }
    public String jsonResults () {
        PointerByReference      bufferRef   = new PointerByReference();
        reLib.REGetDocumentJsonResults      (handle, bufferRef);
        String                  json        = bufferRef.getValue().getString(0);
        reLib.REReleaseString               (bufferRef.getValue());
        return json;
    }

    public String[] categoryKeys () {
        Wrapper.SAStruct        sa          = reLib.REGetDocumentCategoryKeys(handle);
        String[]                keys        = sa.elements.getStringArray(0, sa.count);
        reLib.REReleaseStringArray          ((Wrapper.SAStruct.ByValue)sa);
        return                  keys;
    }
    public String[] stringKeys (String catKey) {
        Wrapper.SAStruct        sa         = reLib.REGetDocumentStringKeys(handle, catKey);
        String[]                keys       = sa.elements.getStringArray(0, sa.count);
        reLib.REReleaseStringArray          ((Wrapper.SAStruct.ByValue)sa);
        return                  keys;
    }
    public String stringForKey (String catKey, String szKey) {
        PointerByReference      ptr         = new PointerByReference();
        reLib.REGetDocumentStringForKey     (handle, catKey, szKey, ptr);
        String                  val         = ptr.getValue().getString(0);
        reLib.REReleaseString               (ptr.getValue());
        return val;
    }
    public void setStringForKey (String category, String key, String value) throws Exception {
        if (handle==null)
            throw new Exception ("The Recognition Document isn't valid.");
        else if (!reLib.RESetDocumentStringForKey(handle, category, key, value))
            throw new Exception("Unable to attach a string to a Recognition Document. ");
    }
    public HashMap<String, HashMap<String, String>> strings () throws Exception {
        HashMap<String, HashMap<String, String>>
                                ret         = new HashMap<>();
        for (String catKey : categoryKeys()) {
            HashMap<String, String>
                                strings     = new HashMap<>();
            for (String szKey : stringKeys(catKey)) {
                strings.put                 (szKey, stringForKey(catKey, szKey));
            }
            ret.put                         (catKey, strings);
        }
        return ret;
    }

    public String[] imagesKeys () throws Exception {
        Wrapper.SAStruct        sa          = reLib.REGetDocumentImageKeys(handle);
        String[]                keys        = sa.elements.getStringArray(0, sa.count);
        reLib.REReleaseStringArray          ((Wrapper.SAStruct.ByValue)sa);
        return                  keys;
    }

    public void setImageFilenameForKey (String key, String filename) throws Exception {
        if (handle==null)
            throw new Exception ("The Recognition Document isn't valid.");
        byte[]      data;
        try {
            Path            path        = Paths.get(filename);
            data                        = Files.readAllBytes(path);
        }
        catch (IOException e) {
            throw new Exception("Unable to read file.");
        }
        setImageForKey                  (key, data);
    }
    public void setImageForKey (String key, byte[] data) throws Exception {
        if (handle==null)
            throw new Exception ("The Recognition Document isn't valid.");
        else if (!reLib.RESetDocumentImageForKey(handle, key, data, data.length))
            throw new Exception("Unable to attach an image to a Recognition Document.");
    }

    public byte[] imageForKey (String key) throws Exception {
        return imageForKey(key, "PNG");
    }
    public byte[] imageForKey (String key, String format) throws Exception {
        LongByReference     rLen    = new LongByReference();
        Pointer             rMem    = new Memory(Pointer.SIZE);
        if (!reLib.REGetDocumentImageForKey(handle, key, format, type, rLen, rMem))
            throw new Exception("Unable to retrieve image for the given key.");
        long                len     = rLen.getValue();
        byte[]              buff    = new byte[(int)len];
        rMem.getPointer(0).read     (0, buff, 0, (int)len);
        reLib.REReleaseImage        (rMem);
        return buff;
    }
}
