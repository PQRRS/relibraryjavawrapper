package fr.PierreQR.RELibrary;

import java.util.*;
import com.sun.jna.*;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.PointerByReference;
import java.lang.reflect.Method;

public class Wrapper {
    public static class SAStruct extends Structure {
        public static   class   ByValue             extends SAStruct implements Structure.ByValue {}
        @Override
        protected       List    getFieldOrder       () {
            return Arrays.asList(new String[] { "count", "elements" });
        }
        public          int     count;
        public          Pointer elements;
    }
    
    private static Map LoadingOptions () {
        HashMap opts = new HashMap<>();
        /*
        opts.put (Library.OPTION_FUNCTION_MAPPER, 
            new FunctionMapper() {
                String osname = System.getProperty("os.name");
                @Override public String getFunctionName (NativeLibrary lib, Method m) {
                    String mn = m.getName();
                    if (!osname.startsWith("Windows"))
                        return mn;
                    switch (mn) {
                        case "REVersion":
                        case "REClientBinaryVersion":
                        case "REStaticInitializer":
                        case "RECreateDocument":
                            return "_" + mn + "@0";
                        case "REReleaseString":
                        case "REReleaseStringArray":
                        case "REReleaseImage":
                        case "RECreateEngine":
                        case "REDeleteDocument":
                        case "REDeleteEngine":
                        case "REGetDocumentCategoryKeys":
                        case "REGetDocumentImageKeys":
                        case "REGetDocumentType":
                            return "_"+mn+"@4";
                        case "REGetDocumentStringKeys":
                        case "REGetDocumentJsonResults":
                        case "RELoadEngineClientBinary":
                            return "_"+mn+"@8";
                        case "REGetDocumentJsonResultForKey":
                        case "REGuessDocumentType":
                        case "RESetDocumentType":
                            return "_"+mn+"@12";
                        case "RERecognizeDocumentData":
                        case "REGetDocumentStringForKey":
                        case "RESetDocumentStringForKey":
                        case "RESetDocumentImageForKey":
                            return "_"+mn+"@16";
                        case "REGetDocumentImageForKey":
                            return "_"+mn+"@24";
                        default:
                            return "_"+mn+"@0";
                    }
                }
            }
        );
        */
        return opts;
    }
    public interface NativeHelper extends com.sun.jna.Library {
        NativeHelper   Instance = (NativeHelper)Native.loadLibrary("RELibrary", NativeHelper.class, LoadingOptions());
        
        void        REStaticInitializer             ();
        String      REVersion                       ();
        String      REClientBinaryVersion           ();
        
        boolean     REReleaseString                 (Pointer stringMemoryHandle);
        boolean     REReleaseStringArray            (SAStruct.ByValue array);
        boolean     REReleaseImage                  (Pointer imageMemoryHandle);
        
        Pointer     RECreateEngine                  (String licenseFilename);
        boolean     RELoadEngineClientBinary        (Pointer reHandle, String clientBinaryPath);
        boolean     REDeleteEngine                  (Pointer reHandle);
        
        Pointer     RECreateDocument                ();
        boolean     REDeleteDocument                (Pointer rdHandle);
        
        boolean     REGetDocumentJsonResults        (Pointer rdHandle, PointerByReference jsonOut);
        boolean     REGetDocumentJsonResultForKey   (Pointer rdHandle, String catKey, PointerByReference jsonOut);

        SAStruct.ByValue
                    REGetDocumentCategoryKeys       (Pointer rdHandle);
        SAStruct.ByValue
                    REGetDocumentStringKeys         (Pointer rdHandle, String catKey);
        boolean     REGetDocumentStringForKey       (Pointer rdHandle, String catKey, String key, PointerByReference stringOut);
        boolean     RESetDocumentStringForKey       (Pointer rdHandle, String catKey, String key, String value);

        SAStruct.ByValue
                    REGetDocumentImageKeys          (Pointer rdHandle);
        boolean     REGetDocumentImageForKey        (Pointer rdHandle, String key, String format, int quality, LongByReference outLength, Pointer imageDataOut);
        boolean     RESetDocumentImageForKey        (Pointer rdHandle, String key, byte[] data, int dataLength);
        boolean     REDeleteDocumentImageForKey     (Pointer rdHandle, String key);
        
        boolean     REGuessDocumentType             (Pointer reHandle, Pointer rdHandle, int parallizedCoresCount);
        int         REGetDocumentType               (Pointer rdHandle);
        boolean     RESetDocumentType               (Pointer reHandle, Pointer rdHandle, int typeCode);
        boolean     RERecognizeDocumentData         (Pointer reHandle, Pointer rdHandle, int parallizedCoresCount, int threadId);
    }
}
