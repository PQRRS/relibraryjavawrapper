package fr.PierreQR.RELibrary;

import com.sun.jna.Pointer;

public class Engine {
    private static final Wrapper.NativeHelper reLib = Wrapper.NativeHelper.Instance;
    private Pointer handle;
    
    public Engine (String licenseFilename) throws Exception {
        handle = reLib.RECreateEngine(licenseFilename);
        if (handle==null)
            throw new Exception("Unable to instanciate a Recognition Engine.");
    }
    public void release () {
        if (handle==null)
            return;
        reLib.REDeleteEngine(handle);
        handle  = null;
    }
    @Override protected void finalize () throws Throwable {
        release();
        super.finalize();
    }
    public void loadClientBinary (String name) throws Exception {
        if (handle==null)
            throw new Exception ("The Recognition Engine isn't valid.");
        else if (!reLib.RELoadEngineClientBinary(handle, name))
            throw new Exception("Loading of Recognition Client Binary has failed.");
    }
    
    public boolean guessDocumentType (Document doc, int parallizedCoresCount) throws Exception {
        if (doc.type!=0)
            return true;
        else if (handle==null)
            throw new Exception ("The Recognition Engine isn't valid.");
        if (!reLib.REGuessDocumentType(handle, doc.handle, parallizedCoresCount))
            return false;
        doc.type                    = reLib.REGetDocumentType(doc.handle);
        return true;
    }
    
    public boolean recognizeDocument (Document doc, int parallizedCoresCount, int threadId) throws Exception {
        if (handle==null)
            throw new Exception ("The Recognition Engine isn't valid.");
        if (doc.type!=0)
            reLib.RESetDocumentType(handle, doc.handle, doc.type);
        return reLib.RERecognizeDocumentData(handle, doc.handle, parallizedCoresCount, threadId);
    }
    public boolean guessAndRecognizeDocument (Document doc, int parallizedCoresCount, int threadId) throws Exception {
        return guessDocumentType(doc, parallizedCoresCount) && recognizeDocument(doc, parallizedCoresCount, threadId);
    }
}
