package fr.PierreQR.RELibrary;

// "C:/home/MainUser/Documents/RecognitionSuite/Unversioned/Projects/REAExports/CRT.recb" "C:/home/MainUser/Documents/RecognitionSuite/Unversioned/Images/WrapperTestInput01.tif"
public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Press a key to continue...");
        System.in.read();
        Wrapper.NativeHelper  reLib         = Wrapper.NativeHelper.Instance;
        
        if (args.length!=3) {
            System.err.println              ("Usage: RELibraryWrapper.jar [License File Path] [Client Binary Path] [Test Picture Path]");
            return;
        }
        String          licFile             = args[0];
        String          cbFile              = args[1];
        String          imgFile             = args[2];
        
        reLib.REStaticInitializer           ();
        
        Engine          eng                 = new Engine(licFile);
        eng.loadClientBinary                (cbFile);
        
        long            start               = System.currentTimeMillis();
        for (int i=0; i<10; ++i) {
            Document        doc             = new Document();
            doc.setImageFilenameForKey      ("Front", imgFile);
            System.out.println              ("Recognition pass #"+i);
            eng.guessAndRecognizeDocument   (doc, 4, 1);
            System.out.println              ("\tStrings: "+doc.strings().keySet().size());
            System.out.println              ("\tImages: "+doc.imagesKeys().length);
            doc.release();
        }
        long                elapsed         = System.currentTimeMillis() - start;
        System.out.println                  ("Elapsed time = " + elapsed + "ms");
        eng.release                         ();
    }
}
